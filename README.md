Create, parse, modify an XML file associated to a transaction.



## INSTALLATION

Resif Data Transfer  published on [PyPI](https://test.pypi.org/project/resif-data-transfer/) and can be installed from there:
```
 pip install -i https://test.pypi.org/simple/ ResifDataTransferTransaction
 ```

Then, you should be able to execute :
```
$ ResifDataTransferTransaction -h
```
